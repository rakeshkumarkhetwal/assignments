operation=$1
location=$2
dirname=$3
content=$4
startrange=$5
endrange=$6

function addDir(){
        mkdir -p $location/$dirname

}


function listFiles(){
	ls -l $location | grep -v '^d' 
}



function listDirs(){
	ls -l $location | grep '^d'
}

function listAll(){
	ls -lA $location
        echo "All Files and Directoires"
}


function deleteDir(){
	rmdir -p $location/$name
}


function addFile(){
	cd $location/$dirname 
	touch $content
}


function addContentToFile(){
	
	echo "$content" >> $location/$dirname/
        echo " content added to end of file"

}

function addContentToFileBegining(){

	echo -e "$content\n$(cat $location/$dirname)" > $location/$dirname
}



function showFileBeginingContent(){
	head -n $startrange $location/$dirname/$content

}


function showFileEndContent(){
	tail -n $startrange

}



function showFileContentAtLine(){

	sed -n "${startrange}p" $location/$dirname/$content

}


function showFileContentForLineRange(){
	sed -n "$startrange,$endrange"p $location/$dirname/$content
}




function moveFile(){
	mv $location/$dirname/$content $startrange/$endrange
}


function copyFile(){
	cp $location/$dirname/$content $startrange/$endrange
}


function clearFileContent(){
	true > $location/$dirname/$content
}




function deleteFile(){
	rm $location/$dirname/$content
}


 
case $operation in
	addDir)
		addDir
		;;
	listFiles)
		listFiles
		;;
	listDirs)
		listDirs
		;;
	listAll)
		listAll
		;;
	deleteDir)
		deleteDir
		;;
	addFile)
		addFile
		;;
	addContentToFile)
		addContentToFile
		;;
	addContentToFileBegining)
		addContentToFileBegining
		;;
	showFileBeginingContent)
		showFileBeginingContent
		;;
	showFileEndContent)
		showFileEndContent
		;;
	showFileContentAtLine)
		showFileContentAtLine
		;;
	showFileContentForLineRange)
		showFileContentForLineRange
		;;
	moveFile)
		moveFile
		;;
	copyFile)
		copyFile
		;;
	clearFileContent)
		clearFileContent
		;;
	deleteFile)
		deleteFile
		;;
	
	*)
		echo "Sorry, I don't understand"
		;;
  esac




