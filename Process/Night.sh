#!bin/bash
# It is deleting or emptying the content of process.log file when time is 12 am and creating a temp zip folder inside the temp directory

# Running a cron job as : @midnight rakesh /bin/bash/Night.sh
now="$(date)"

sudo zip -r /tmp/process$now.zip /var/log/process.log
sudo true > /var/log/process.log
